# -*- coding: utf-8 -*-
import os, os.path, sys, time, datetime
import json
import numpy as np
from keras.preprocessing.image import ImageDataGenerator

with open('./config.json') as json_data_file:
  config = json.load(json_data_file)

is_dev_env = config["env"] == "dev"

batch_size = 8 if is_dev_env else 32
epochs = 4 if is_dev_env else 50

def class_count(dir_path='./data/train'):
  cnt = 0
  for f in os.listdir(dir_path):
    if not f.startswith('.'):
      cnt = cnt + 1

  return cnt

def sample_count(dir_path='./data/train'):
  if is_dev_env:
    return 10

  total = 0

  for r, d, files in os.walk(dir_path):
    files = [f for f in files if not f[0] == '.']
    total = total + len(files)

  return total

def model_name(model_name):
  ts = time.time()
  time_str = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d_%H%M%S')

  return "{}_{}.h5".format(model_name, time_str)

def data_dir(train_dir = None, validation_dir = None, test_dir = None):
  if train_dir == None:
    train_dir = config["learning"]["train_dir"]

  if validation_dir == None:
    validation_dir = config["learning"]["validation_dir"]

  if test_dir == None:
    test_dir = config["learning"]["test_dir"]

  return(train_dir, validation_dir, test_dir)

def apply_mean(image_data_generator):
  """Subtracts the dataset mean"""
  image_data_generator.mean = np.array([103.939, 116.779, 123.68], dtype=np.float32).reshape((3, 1, 1))

def train_generator(img_height, img_width, batch_size, train_data_dir):
  train_datagen =  ImageDataGenerator(
    #rotation_range=90.,
    #width_shift_range=0.2,
    #height_shift_range=0.2,
    #shear_range=0.3,
    #zoom_range=0.3,
    #horizontal_flip=True
    )

  apply_mean(train_datagen)

  train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size = (img_height, img_width),
    batch_size = batch_size,
    class_mode = "categorical")

  return train_generator

def validation_generator(img_height, img_width, batch_size, validation_data_dir):
  validation_datagen = ImageDataGenerator()

  apply_mean(validation_datagen)

  validation_generator = validation_datagen.flow_from_directory(
    validation_data_dir,
    target_size = (img_height, img_width),
    class_mode = "categorical")

  return validation_generator
