import os, os.path, sys, getopt
import numpy as np
import json
import math

from keras.models import Sequential, Model
from keras.layers import Input, Activation, Dropout, Flatten, Dense, AveragePooling2D, GlobalAveragePooling2D
from keras import optimizers, metrics
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, TensorBoard, EarlyStopping

import dataset_helper as DatasetHelper

with open('./config.json') as json_data_file:
  config = json.load(json_data_file)

def get_train_model(model_name, size, nb_classes):
  if model_name == "vgg16":
    from keras.applications.vgg16 import VGG16
    print('Build VGG16 Model')

    model = VGG16(weights = "imagenet",
      include_top=False,
      input_shape = (size, size, 3))
    model = Sequential(layers=model.layers)
    model.add(Flatten())
    model.add(Dense(2048))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2048))
    model.add(Activation('relu'))
    model.add(Dense(nb_classes))
    model.add(Activation('softmax'))

    return model
  elif model_name == "inception_v4":
    import applications.inception_v4 as InceptionV4
    print('Build Inception_V4 Model')

    base_model = InceptionV4.create_model(nb_classes, weights='imagenet', include_top=False)

    x = base_model.output
    x = AveragePooling2D((8,8), padding='valid')(x)
    x = Dropout(0.5)(x)
    x = Flatten()(x)
    predictions = Dense(units=nb_classes, activation='softmax')(x)
    model = Model(inputs=base_model.input, outputs=predictions)

    return model

  elif model_name == "resnet50":
    from keras.applications.resnet50 import ResNet50
    print('Build ResNet50 Model')

    base_model = ResNet50(weights='imagenet',
      include_top=False,
      input_tensor=Input(shape=(size, size) + (3,))
      )
    x = base_model.output
    x = Flatten()(x)
    x = Dropout(0.5)(x)
    x = Dense(2048, activation='relu', name='fc1')(x)
    x = Dropout(0.5)(x)
    predictions = Dense(nb_classes, activation='softmax', name='predictions')(x)
    model = Model(input=base_model.input, output=predictions)

    return model

  elif model_name == "inception_resnet_v2":
    from keras.applications.inception_resnet_v2 import InceptionResNetV2
    print('Build Inception_Resnet_V2 Model')

    base_model = InceptionResNetV2(weights='imagenet',
      include_top=False,
      input_tensor=Input(shape=(size, size) + (3,))
      )
    x = base_model.output
    x = GlobalAveragePooling2D(name='avg_pool')(x)
    predictions = Dense(nb_classes, activation='softmax')(x)
    model = Model(inputs=base_model.input, outputs=predictions)

    return model

def train(model_name):
  size = config["learning"]["models"][model_name]["size"]
  img_width, img_height = size, size

  (train_data_dir, validation_data_dir, test_data_dir) = DatasetHelper.data_dir()
  nb_classes = DatasetHelper.class_count(train_data_dir)
  nb_train_samples = DatasetHelper.sample_count(train_data_dir)
  nb_validation_samples = DatasetHelper.sample_count(validation_data_dir)
  nb_test_samples = DatasetHelper.sample_count(test_data_dir)

  batch_size = DatasetHelper.batch_size
  epochs = DatasetHelper.epochs

  train_generator = DatasetHelper.train_generator(img_height,
    img_width,
    batch_size,
    train_data_dir)

  validation_generator = DatasetHelper.validation_generator(img_height,
    img_width,
    batch_size,
    validation_data_dir)

  class_indices = [None]*nb_classes
  for key, value in train_generator.class_indices.items():
    class_indices[value] = key

  model = get_train_model(model_name, size, nb_classes)

  model.compile(loss = "categorical_crossentropy",
    optimizer = Adam(lr=0.0001),
    metrics=["accuracy", 'top_k_categorical_accuracy'])

  model.fit_generator(
    train_generator,
    steps_per_epoch = math.ceil(nb_train_samples / batch_size),
    epochs = epochs,
    validation_data = validation_generator,
    validation_steps = math.ceil(nb_validation_samples / batch_size))

  model_file_name = DatasetHelper.model_name(model_name)
  model_store_path = "./models/{}".format(model_file_name)

  model.save(model_store_path)

def main(argv):
  model_name = None

  try:
    opts, args = getopt.getopt(argv, "m:", ["model_name="])
  except getopt.GetoptError as err:
    print('model 이름은 필수 입니다. (vgg16, inception_v4, resnet50, inception_resnet_v2)')

  for opt, arg in opts:
    if opt in ("-m", "--model_name"):
      model_name = arg

  if model_name != None:
    train(model_name)

main(sys.argv[1:])
